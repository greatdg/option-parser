class OptionParser {
	constructor() {
		this.values = [];
		this.options = {};
	}

	_getFlag(flag) {
		return this.options[flag] ? flag : this._findAlias(flag);
	}

	_findAlias(flag) {
		let alias = Object.keys(this.options).filter(key => {
			return flag === this.options[key].alias;
		});
		return alias[0] || false;
	}

	_typeCheck(flag) {
		return this.options[flag] && typeof this.options[flag].value === 'boolean';
	}

	_setValue(flag, value) {
		if (!this.options[flag]) return;

		if (typeof this.options[flag].value === 'boolean') {
			this.options[flag].value = value;
		} else {
			this.options[flag].value.push(value);
		}
	}

	addStringOption(flags) {
		if (!flags) return;

		let separateFlags = flags.split(' ');
		return (this.options[separateFlags[0]] = {
			value: [],
			alias: separateFlags[1] || ''
		});
	}

	addBoolOption(flags) {
		if (!flags) return;

		let separateFlags = flags.split(' ');
		return (this.options[separateFlags[0]] = {
			value: false,
			alias: separateFlags[1] || ''
		});
	}

	isSet(flag) {
		flag = this._getFlag(flag);
		if (!flag) return;

		return this._typeCheck(flag)
			? this.options[flag].value
			: this.options[flag].value.length > 0;
	}

	get(flag) {
		flag = this._getFlag(flag);
		if (!flag) return;

		return this._typeCheck(flag)
			? this.options[flag].value
			: this.options[flag].value[0];
	}

	getAll(flag) {
		flag = this._getFlag(flag);
		if (!this.options[flag]) return;

		return this.options[flag].value;
	}

	reset() {
		this.values = [];
		for (let flag in this.options) {
			this.options[flag].value = this._typeCheck(flag) ? false : [];
		}
	}

	parse(args) {
		let pass = false;

		args.forEach((arg, i, arr) => {
			// check if pass is false and arg isn't include, this is real value
			if (arg.indexOf('-') === -1 && pass === false) {
				this.values.push(arg);
			} else if (arg.indexOf('-') === 0 && pass === false) {
				// get the flag
				let getFlag = (arg.substring(0, 2) === '--'
					? arg.slice(2)
					: arg.slice(1)
				).match(/^(\w+)/g)[0];

				// get the value following that flag
				let re = new RegExp(`^(-|--)${getFlag}=?`, 'g');
				let value = arg.replace(re, '');

				// check if flag does not exist in options, considers either alias or juxtaposition
				if (!this.options[getFlag]) {
					let alias = this._findAlias(getFlag);

					// check if cannot find alias, consider juxtaposition of boolean flags
					if (!alias) {
						for (let flag of getFlag.split('')) {
							if (this._typeCheck(flag)) {
								this._setValue(flag, true);
							}
						}
					} else {
						getFlag = alias;
					}
				}

				if (this.options[getFlag]) {
					// check if the value of flag is boolean
					if (this._typeCheck(getFlag)) {
						value = true;
					} else if (value.length === 0) {
						// check if the value of flag is empty, get the value from next index of array and skip next iteration
						value = arr[i + 1] || arr[arr.length - 1];
						pass = true;
					}
					this._setValue(getFlag, value);
				}
			} else {
				pass = false;
			}
		});

		return this.values;
	}
}

module.exports = OptionParser;
